<?php
/**
 * @file
 * Template for Radix Bartlett.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display bartlett clearfix <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="container-fluid">
    <div class="row">

      <!-- Sidebar -->
      <div class="col-md-4 radix-layouts-sidebar panel-panel bartlett-sidebar bartlett-column-content-region bartlett-column bartlett-container">
        <div class="panel-panel-inner bartlett-sidebar-inner bartlett-column-content-region-inner bartlett-column-inner bartlet-container-inner">
          <?php print $content['sidebar']; ?>
        </div>
      </div>

      <div class="col-md-8 bartlett-content-container bartlett-container">
        <div class="row">
          <div class="col-md-12 radix-layouts-contentheader panel-panel bartlett-column-content-region bartlett-content-header">
            <div class="panel-panel-inner bartlett-column-content-region-inner bartlett-content-header-inner">
              <?php print $content['contentheader']; ?>
            </div>
          </div>
          <div class="col-md-6 radix-layouts-contentcolumn1 panel-panel bartlett-column-content-region bartlett-content-column1 bartlett-column">
            <div class="panel-panel-inner bartlett-column-content-region-inner bartlett-content-column1-inner bartlett-column-inner">
              <?php print $content['contentcolumn1']; ?>
            </div>
          </div>
          <div class="col-md-6 radix-layouts-contentcolumn2 panel-panel bartlett-column-content-region bartlett-content-column2 bartlett-column">
            <div class="panel-panel-inner bartlett-column-conten-region-inner bartlett-content-column2-inner bartlett-column-inner">
              <?php print $content['contentcolumn2']; ?>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>  
</div><!-- /.bartlett -->
